import React from 'react';
import * as service from '../services';
import Card from 'react-bootstrap/Card';
import ListGroup from 'react-bootstrap/ListGroup';

class Horror extends React.Component {
  constructor(props) {
    super(props);
    this.state={data: []}
  }

  componentDidMount(){
    service.getHorrorBooks({}, (err, res, body) => {debugger
      let data = JSON.parse(body);

      this.setState({ data: data.data })
    })
  }

  render(){
    return (
      <Card style={{ width: '18rem' }}>
        <Card.Header>Available Horror Books = {this.state.data.length}</Card.Header>
        <ListGroup variant="flush">
          {this.state.data.map((book) => {debugger
            return <ListGroup.Item variant="primary"><li key="{book.id}">{book.bookname}</li></ListGroup.Item>
          })}
        </ListGroup>
      </Card>
    );
  }
}


export default Horror;
