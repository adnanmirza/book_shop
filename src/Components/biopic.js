import React from 'react';
import * as service from '../services';
import { Image, Card } from 'react-bootstrap';
import ListGroup from 'react-bootstrap/ListGroup';
import Container from 'react-bootstrap/Container';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';


class Biopic extends React.Component{
  constructor(props) {
    super(props);

    this.state={ data: [] }
  }

  componentDidMount(){
    service.getBiopics({}, (err, res, body) => {
      let data = JSON.parse(body);

      this.setState({ data: data.data});
    })
  }

  render(){
    return(
      <Row md="3">
        {
          this.state.data.map((book) => {
          return (
            <Col md="3">
              <Card >
                <Card.Body>
                  <Image variant="top" src={require("../images/" + book.image )} height="200rm"/>
                  <Card.Title>{book.bookname}</Card.Title>
                </Card.Body>
              </Card>
            </Col>
          )
          })
        }
      </Row>
    )
  }

}

export default Biopic;
