import React from 'react';
import Card from 'react-bootstrap/Card';
import * as service from '../services';
import ListGroup from 'react-bootstrap/ListGroup';
import AutoBiography from './autobiography';

class Fiction extends React.Component {
  constructor(props){
    super();
    this.state={ data: [] }
  }

  componentDidMount(){
    service.getFictionBooks({}, (err, res, body) =>{
      let data = JSON.parse(body);

      this.setState({ data: data.data })
    })
  }

  render(){
    return (
      <Card style={{ width: '18rem' }}>
        <Card.Body>
          <Card.Title>Available Fiction books = {this.state.data.length}</Card.Title>
          <Card.Text>
            Fiction storirs written by famous writers.
          </Card.Text>
        </Card.Body>
        <ListGroup variant="flush">
          {this.state.data.map((book) => {
            return <ListGroup.Item variant="primary"><li key="{book.id}">{book.bookname}</li></ListGroup.Item>
          })}
        </ListGroup>
        <Card.Body>
          <Card.Link href="./autobiography.js">Card Link</Card.Link>
          <Card.Link href="#">Another Link</Card.Link>
        </Card.Body>
      </Card>
    );
  }

}


export default Fiction;
