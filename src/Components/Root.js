import React from 'react';
import ReactDOM from 'react-dom';
import { lazy, Suspense } from 'react';
import {createBrowserHistory} from 'history';
import { Route, Link, NavLink, BrowserRouter as Router, Switch, } from 'react-router-dom';
import { Button, Navbar, Nav, NavDropdown } from 'react-bootstrap';
import Notfound from './notfound';
import * as service from '../services';

const Horror = lazy(() => import('./horror'))
const Biopic = lazy(() => import('./biopic'))
const Fiction  = lazy(() => import('./fiction'))
const AutoBiography = lazy(() => import('./autobiography'))

const createHistory = createBrowserHistory()

const WaitingComponent = (Component) => {debugger
  return props => (
    <Suspense fallback={<div>Loading...</div>}>
      <Component {...props} />
    </Suspense>
  );
}

class Root extends React.Component {
  constructor(props) {
    super(props);

    this.state={ data: [] }
  }

  componentDidMount() {
    service.getCategories({}, (err, res, body) => {
      let data = JSON.parse(body)

      this.setState({ data: data.data })
    })
  }

  render() {
    return (
      <Router>
        <div className="justify-content-md-center">
          <h1>Categories</h1>
          <Navbar bg="primary" variant="dark">
            <Navbar.Brand href="./">Navbar</Navbar.Brand>
            <Nav className="mr-auto">
              {this.state.data.map((category) => {
                return (
                  <Nav.Link href={"/"+category.name.toLowerCase()}>{category.name}</Nav.Link>
                )
              })
              }
              <NavDropdown title="Categories" id="collasible-nav-dropdown">
                {this.state.data.map((category) => {
                  return (
                    <NavDropdown.Item href={"/"+category.name.toLowerCase()}>{category.name}</NavDropdown.Item>
                  )
                })
                }
              </NavDropdown>
            </Nav>
          </Navbar>
          <Switch>
            <Route exact path="./" component={Root} />
            <Route exact path="/fiction" component={WaitingComponent(Fiction)} />
            <Route exact path="/biopic" component={WaitingComponent(Biopic)} />
            <Route exact path="/autobiography" component={WaitingComponent(AutoBiography)} />
            <Route exact path="/horror" component={WaitingComponent(Horror)} />
            <Route render={() => <Notfound something={'Page Not Found'} />} />
          </Switch>
        </div>
    </Router>
    );
  }
}

export default Root;
