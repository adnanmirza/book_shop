import React from 'react'
import request from 'request'



export function getBooks(params, callback){
  const options = {
    method: 'GET',
    url: 'http://demo7770451.mockable.io/autobiographies',
    headers: {
     'content-type' : 'application/json',
    },
  };

  request(options, callback);
}

export function getBiopics(params, callback){debugger
  const options = {
    method: 'GET',
    url: 'http://demo7770451.mockable.io/biopics',
    headers: {
     'content-type' : 'application/json',
    },
  }
  request(options, callback)
}

export function getFictionBooks(params, callback){debugger
  const options = {
    method: 'GET',
    url: 'http://demo7770451.mockable.io/fiction',
    headers: {
     'content-type' : 'application/json',
    },
  }
  request(options, callback)
}

export function getHorrorBooks(params, callback){
  const option = {
    method: 'GET',
    url: 'http://demo7770451.mockable.io/horror',
    headers: {
     'content-type' : 'application/json',
    },
  }
  request(option, callback)
}

export function getCategories(params, callback){
  const option = {
    method: 'GET',
    url: 'http://demo7770451.mockable.io/categories',
    headers: {
     'content-type' : 'application/json',
    },
  }
  request(option, callback)
}
